import * as VDom from 'src/lib/vdom';

export function sampleHOC<C extends VDom.Component<{ value: number }>>(
  Component: C
): VDom.Component<C extends VDom.Component<infer P> ? Partial<P> : never> {
  let value = 0;

  return (props) => {
    value += 1;
    return Component({ ...props, value });
  };
}
