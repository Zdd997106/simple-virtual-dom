import { render } from './render';
import { VNode } from './types';

export function mount(vdom: VNode) {
  return ($node: Element | Text) => {
    const newNode = render(vdom);

    $node.replaceWith(newNode);

    return newNode;
  };
}
