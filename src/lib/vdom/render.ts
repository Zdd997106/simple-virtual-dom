import { VDom, VNode } from './types';

function renderVDom({ tagName, attrs, children }: VDom) {
  const element = document.createElement(tagName);

  Object.entries(attrs).forEach(([key, value]) => {
    element.setAttribute(key, String(value));
  });

  children.forEach((child) => {
    element.appendChild(render(child));
  });

  return element;
}

export function render(vnode: VNode) {
  if (typeof vnode === 'string') {
    return document.createTextNode(vnode);
  }

  if (typeof vnode === 'number') {
    return document.createTextNode(vnode.toString());
  }

  if (typeof vnode === 'object' && vnode !== null) {
    return renderVDom(vnode);
  }

  return undefined;
}
