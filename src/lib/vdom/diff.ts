import { render } from './render';
import { VNode } from './types';

export function diff(currentVNode: VNode | undefined, newVNode: VNode | undefined): Patch {
  if (!newVNode) {
    return ($node) => {
      $node.remove();
      return $node;
    };
  }

  if (
    !currentVNode ||
    typeof newVNode === 'string' ||
    typeof newVNode === 'number' ||
    typeof currentVNode === 'string' ||
    typeof currentVNode === 'number'
  ) {
    if (currentVNode === newVNode) return pass;
    return resetToNew(newVNode);
  }

  if (typeof newVNode === 'number') {
    return resetToNew(newVNode);
  }

  if (currentVNode.tagName !== newVNode.tagName) {
    return resetToNew(newVNode);
  }

  if (currentVNode.tagName === newVNode.tagName) {
    const attributesPatch = attributesDiff(currentVNode.attrs, newVNode.attrs);
    const childrenPatch = childrenDiff(currentVNode.children, newVNode.children);

    return ($node) => {
      attributesPatch($node);
      childrenPatch($node);

      return $node;
    };
  }

  return pass;
}

function resetToNew(newVNode: VNode) {
  return ($node: Element | Text) => {
    const newNode = render(newVNode);
    $node.replaceWith(newNode);
    return newNode;
  };
}

function pass($node) {
  return $node;
}

type Patch = {
  ($node: Element | Text): Element | Text;
};

type PatchWithoutChangeNode = {
  ($node: Element | Text): void;
};

function attributesDiff(
  currentAttributes: Record<string, unknown>,
  newAttributes: Record<string, unknown>
): PatchWithoutChangeNode {
  const patchStack: PatchWithoutChangeNode[] = [];

  Object.keys(newAttributes).forEach((key) => {
    if (currentAttributes[key] !== newAttributes[key]) {
      patchStack.push(($node) => {
        ($node as Element).setAttribute(key, `${newAttributes[key]}`);
      });
    }
  });

  Object.keys(currentAttributes).forEach((key) => {
    if (!(key in newAttributes)) {
      patchStack.push(($node) => {
        ($node as Element).removeAttribute(key);
      });
    }
  });

  return ($node) => {
    patchStack.forEach((patch) => {
      patch($node);
    });
  };
}

function childrenDiff(currentChildren: VNode[], newChildren: VNode[]): PatchWithoutChangeNode {
  const patchStack: PatchWithoutChangeNode[] = [];

  currentChildren.forEach((_, i) => {
    const currentChildVNode = currentChildren[i];
    const newChildVNode = newChildren[i];
    const childPatch = diff(currentChildVNode, newChildVNode);
    patchStack.push(($parentNode) => {
      childPatch($parentNode.childNodes[i] as Element | Text);
    });
  });

  newChildren.forEach((_, i) => {
    if (!currentChildren[i]) {
      patchStack.push(($parentNode) => {
        $parentNode.appendChild(render(newChildren[i]));
      });
    }
  });

  return ($node) => {
    patchStack.forEach((patch) => {
      patch($node);
    });
  };
}
