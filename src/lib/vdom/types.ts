export interface VDom {
  tagName: string;
  attrs: Record<string, unknown>;
  children: VNode[];
}

export type VNode = VDom | string | number | undefined | null;

export interface Component<P = any> {
  (props?: P): VNode;
}
