import { VDom } from './types';

export function createElement(
  tagName: VDom['tagName'],
  attrs: VDom['attrs'] = {},
  ...children: VDom['children']
): VDom {
  return {
    tagName,
    attrs,
    children,
  };
}
