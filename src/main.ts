import SampleComponent from 'src/components/Sample';
import * as VDom from './lib/vdom';

let root: Element | Text = document.getElementById('root');
let currentVNode;

setInterval(() => {
  const newVNode = SampleComponent();
  const patch = VDom.diff(currentVNode, newVNode);

  root = patch(root);
  currentVNode = newVNode;
}, 1000);
