import * as VDom from 'src/lib/vdom';
import { sampleHOC } from 'src/hoc/sampleHOC';

const SampleComponent: VDom.Component<{ value: number }> = ({ value }) =>
  VDom.createElement(
    // tagName
    'div',

    // attributes
    { id: 'app' },

    // childrens
    VDom.createElement('p', {}, value),
    VDom.createElement('input', { type: 'text' })
  );

export default sampleHOC(SampleComponent);
